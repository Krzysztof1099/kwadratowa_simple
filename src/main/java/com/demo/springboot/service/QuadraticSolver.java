package com.demo.springboot.service;

import com.demo.springboot.model.QuadraticEquasionSolution;

public interface QuadraticSolver {
    public QuadraticEquasionSolution solveEquasion(float a, float b, float c);
}
